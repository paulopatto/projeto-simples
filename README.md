# Projeto simples

Readme do projeto simples para tutorial de **ansible**


### Vagrant
Para instalar o vagrant:

- baixe o mesmo da pagina https://www.vagrantup.com/downloads.html.
- instale com o comando `sudo dpkg --install PATH_TO_FILE.deb`
- baixe uma box com o comando `vagrant box add "ubuntu/trusty64"` por exemplo
- na pasta onde tem o seu *Vagrantfile* use o comando `vagrant up && vagrant ssh`
